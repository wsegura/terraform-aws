
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["Amazon Linux 2*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["99999999999"] # Canonical
}

resource "aws_instance" "web" {
  ami = data.aws_ami.ubuntu.id

  instance_type = "t3a.nano"
  availability_zone = "us-east-1a"


  tags = {
    Name = "servidor-aws-linux"
  }
}

resource "aws_ebs_volume" "volumen" {
  availability_zone = aws_instance.web.availability_zone
  size              = 20
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.volumen.id
  instance_id = aws_instance.web.id
}